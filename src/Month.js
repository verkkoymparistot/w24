import React, { useState } from 'react';
import Day from './Day';

const Month = () => {
  const [selectedMonth, setSelectedMonth] = useState(0); 

  const getFirstDayOfWeek = (month) => {
    const firstDayOfMonth = new Date(2021, month, 1);
    return firstDayOfMonth.getDay();
  };

  const getMonthName = (month) => {
    const monthNames = [
      'January', 'February', 'March', 'April',
      'May', 'June', 'July', 'August',
      'September', 'October', 'November', 'December'
    ];
    return monthNames[month];
  };

  return (
    <div>
      <select value={selectedMonth} onChange={(e) => setSelectedMonth(parseInt(e.target.value, 10))}>
        {Array.from({ length: 12 }, (_, i) => i).map((month) => (
          <option key={month} value={month}>{month + 1}</option>
        ))}
      </select>
      <p>{`${getMonthName(selectedMonth)} 2021 starts ${Day({ dayOfWeek: getFirstDayOfWeek(selectedMonth) })}`}</p>
    </div>
  );
};

export default Month;
