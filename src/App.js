import './App.css';
import React from 'react';
import Month from './Month';

function App() {
  return (
    <div className="App">
      <Month />
    </div>
  );
}

export default App;
